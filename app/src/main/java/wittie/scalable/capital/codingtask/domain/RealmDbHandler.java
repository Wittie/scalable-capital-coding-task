package wittie.scalable.capital.codingtask.domain;

import android.support.annotation.NonNull;

import java.util.List;

import io.realm.Realm;
import io.realm.internal.IOException;
import wittie.scalable.capital.codingtask.data.model.OnlineRepository;

public class RealmDbHandler {

    Realm realm;

    public RealmDbHandler(Realm realm) {
        this.realm = realm;
    }

    @NonNull
    public List<OnlineRepository> getListOfOnlineRepositories() {
        return realm.where(OnlineRepository.class).findAll();
    }

    public void deleteAllRepositories() {
        realm.beginTransaction();
        realm.delete(OnlineRepository.class);
        realm.commitTransaction();
    }

    public boolean copyOrUpdate(List<OnlineRepository> repositories) {

        realm.beginTransaction();
        try {
            realm.copyToRealmOrUpdate(repositories);
            realm.commitTransaction();
            return true;
        } catch (IOException e) {
            realm.cancelTransaction();
            return false;
        }
    }

    public long countOnlineRepositories() {
        return realm.where(OnlineRepository.class).count();
    }

}
