package wittie.scalable.capital.codingtask.data.downloaders.events;

public final class OnErrorDownloadingCommit {

    private final Throwable throwable;
    private final String repositoryName;

    public OnErrorDownloadingCommit(Throwable throwable, String repositoryName) {
        this.throwable = throwable;
        this.repositoryName = repositoryName;
    }

    public Throwable getThrowable() {
        return throwable;
    }

    public String getRepositoryName() {
        return repositoryName;
    }
}
