package wittie.scalable.capital.codingtask.data.downloaders;

import android.util.Log;

import org.greenrobot.eventbus.EventBus;

import java.net.UnknownHostException;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import wittie.scalable.capital.codingtask.ScalableCapitalApplication;
import wittie.scalable.capital.codingtask.data.EndPointInterface;
import wittie.scalable.capital.codingtask.data.downloaders.events.OnErrorDownloadingRepositories;
import wittie.scalable.capital.codingtask.data.downloaders.events.OnErrorSavingRepositoriesOnDataBase;
import wittie.scalable.capital.codingtask.data.downloaders.events.OnSuccessDownloadingRepositories;
import wittie.scalable.capital.codingtask.data.model.OnlineRepository;
import wittie.scalable.capital.codingtask.domain.RealmDbHandler;

public class RepositoriesDownloader implements Callback<List<OnlineRepository>> {

    private static final String TAG = RepositoriesDownloader.class.getSimpleName();

    @Inject
    EventBus eventBus;

    @Inject
    EndPointInterface apiService;

    @Inject
    RealmDbHandler dbHandler;

    public RepositoriesDownloader() {
        ScalableCapitalApplication.app().basicComponent().inject(this);
    }

    public void getOnlineRepositories() {
        Call<List<OnlineRepository>> call = apiService.getOnlineRepositories();
        call.enqueue(this);
    }

    @Override
    public void onResponse(Call<List<OnlineRepository>> call, Response<List<OnlineRepository>> response) {
        Log.d(TAG, "onResponse: " + response.body());
        boolean success = dbHandler.copyOrUpdate(response.body());

        if (success) {
            Log.d(TAG, "onResponse: Successfully saved " + dbHandler.countOnlineRepositories() + " repositories");
            eventBus.post(new OnSuccessDownloadingRepositories());
        } else {
            Log.d(TAG, "onResponse: Error Saving On Data Base");
            eventBus.post(new OnErrorSavingRepositoriesOnDataBase());
        }
    }

    @Override
    public void onFailure(Call<List<OnlineRepository>> call, Throwable t) {
        if (t instanceof UnknownHostException) {
            Log.d(TAG, "No Internet: Are you sure you are connected?");
        } else {
            Log.d(TAG, "onFailure: " + t.getLocalizedMessage());
            eventBus.post(new OnErrorDownloadingRepositories(t));
        }
    }

}
