package wittie.scalable.capital.codingtask.data.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public final class CommitsListEntry {

    private String sha;

    private CommitInfo commit;

    private String url;

    @SerializedName("html_url")
    private String htmlUrl;

    @SerializedName("comments_url")
    private String commentsUrl;

    private OnlineRepositoryOwner author;

    private OnlineRepositoryOwner committer;

    private List<Object> parents;

    public String getURL() {
        return htmlUrl;
    }

}
