package wittie.scalable.capital.codingtask.presentation.viewmodel;

import android.content.Intent;
import android.databinding.BaseObservable;
import android.net.Uri;
import android.util.Log;
import android.view.View;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import javax.inject.Inject;

import wittie.scalable.capital.codingtask.ScalableCapitalApplication;
import wittie.scalable.capital.codingtask.data.downloaders.CommitsDownloader;
import wittie.scalable.capital.codingtask.data.downloaders.events.OnErrorDownloadingCommit;
import wittie.scalable.capital.codingtask.data.downloaders.events.OnSuccessDownloadingCommit;
import wittie.scalable.capital.codingtask.data.model.CommitsListEntry;
import wittie.scalable.capital.codingtask.data.model.OnlineRepository;

public final class RepositoryViewModel extends BaseObservable {

    private static final String TAG = RepositoryViewModel.class.getSimpleName();

    @Inject
    EventBus eventBus;

    @Inject
    CommitsDownloader commitsDownloader;

    private OnlineRepository repository;

    private boolean isOpened;

    private CommitsListEntry lastCommit;

    public RepositoryViewModel() {
        ScalableCapitalApplication.app().basicComponent().inject(this);
        eventBus.register(this);
    }

    @Override
    protected void finalize() throws Throwable {
        eventBus.unregister(this);
        super.finalize();
    }


    public void setRepository(OnlineRepository repository) {
        this.repository = repository;

        if (lastCommit == null) {
            commitsDownloader.getCommitsForRepository(repository.getName());
        }
    }

    public String getRepositoryName() {
        return repository.getName();
    }

    public String getOwnerName() {
        return repository.getOwnerName();
    }

    public int lastCommitVisibility() {
        return lastCommit !=  null ? View.VISIBLE : View.GONE;
    }

    public int isOpenedVisibility() {
        return isOpened ? View.VISIBLE : View.GONE;
    }

    public boolean isOpened() {
        return isOpened;
    }

    public void setIsOpened(boolean isOpened) {
        this.isOpened = isOpened;
        notifyChange();
    }

    public View.OnClickListener getOnRepositoryClickListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(repository.getRepositoryUrl()));
                v.getContext().startActivity(browserIntent);
            }
        };
    }

    public View.OnClickListener getOnOwnerClickListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(repository.getOwnerUrl()));
                v.getContext().startActivity(browserIntent);
            }
        };
    }

    public View.OnClickListener getOnLastCommitUrlClickListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (lastCommit != null) {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(lastCommit.getURL()));
                    v.getContext().startActivity(browserIntent);
                }
            }
        };
    }

    @Subscribe
    public void onMessageEvent(OnSuccessDownloadingCommit event) {
        Log.d(TAG, "onMessageEvent: OnSuccessDownloadingCommit: " + event.getRepositoryName());
        this.lastCommit = event.getLastCommit();
        notifyChange();
    }

    @Subscribe
    public void onMessageEvent(OnErrorDownloadingCommit event) {
        Log.d(TAG, "onMessageEvent: OnErrorDownloadingCommit: " + event.getRepositoryName());
    }

}
