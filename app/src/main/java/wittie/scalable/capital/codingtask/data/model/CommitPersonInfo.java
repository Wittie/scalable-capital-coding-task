package wittie.scalable.capital.codingtask.data.model;

import java.util.Date;

public final class CommitPersonInfo {

    private String name;
    private String email;
    private Date date;

}
