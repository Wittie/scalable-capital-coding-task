package wittie.scalable.capital.codingtask.presentation.view;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.View;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.List;

import javax.inject.Inject;

import wittie.scalable.capital.codingtask.R;
import wittie.scalable.capital.codingtask.ScalableCapitalApplication;
import wittie.scalable.capital.codingtask.data.downloaders.RepositoriesDownloader;
import wittie.scalable.capital.codingtask.data.downloaders.events.OnErrorDownloadingRepositories;
import wittie.scalable.capital.codingtask.data.downloaders.events.OnErrorSavingRepositoriesOnDataBase;
import wittie.scalable.capital.codingtask.data.downloaders.events.OnSuccessDownloadingRepositories;
import wittie.scalable.capital.codingtask.data.model.OnlineRepository;
import wittie.scalable.capital.codingtask.databinding.ActivityMainBinding;
import wittie.scalable.capital.codingtask.domain.RealmDbHandler;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();

    @Inject
    EventBus eventBus;

    @Inject
    RealmDbHandler dbHandler;

    @Inject
    RepositoriesDownloader repositoriesDownloader;

    final RecyclerViewAdapter adapter = new RecyclerViewAdapter();

    ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ScalableCapitalApplication.app().basicComponent().inject(this);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        binding.recyclerView.setLayoutManager(new LinearLayoutManager(this));
        binding.recyclerView.setAdapter(adapter);

        reloadRepositories();
    }

    private void reloadRepositories() {
        dbHandler.deleteAllRepositories();
        repositoriesDownloader.getOnlineRepositories();
    }

    public void refreshButtonPressed(View view) {
        reloadRepositories();
    }

    @Override
    protected void onResume() {
        super.onResume();
        eventBus.register(this);
    }

    @Override
    protected void onPause() {
        eventBus.unregister(this);
        super.onPause();
    }

    @Subscribe
    public void onMessageEvent(OnSuccessDownloadingRepositories event) {
        Log.d(TAG, "onMessageEvent: OnSuccessDownloadingRepositories");
        final List<OnlineRepository> repositoriesList = dbHandler.getListOfOnlineRepositories();
        adapter.setRepositories(repositoriesList.toArray(new OnlineRepository[repositoriesList.size()]));
        checkEmptyState();
    }

    @Subscribe
    public void onMessageEvent(OnErrorDownloadingRepositories event) {
        Log.d(TAG, "onMessageEvent: OnErrorDownloadingRepositories");
        checkEmptyState();
    }

    @Subscribe
    public void onMessageEvent(OnErrorSavingRepositoriesOnDataBase event) {
        Log.d(TAG, "onMessageEvent: OnErrorSavingRepositoriesOnDataBase");
        checkEmptyState();
    }

    public void checkEmptyState() {
        if (shouldShowEmptyState()) {
            showEmptyState();
        } else {
            hideEmptyState();
        }
    }

    public boolean shouldShowEmptyState() {
        return adapter.getItemCount() == 0;
    }

    public void hideEmptyState() {
        binding.emptyStateImageView.setVisibility(GONE);
        binding.emptyStateTextView.setVisibility(GONE);
        binding.emptyStateButton.setVisibility(GONE);
    }

    public void showEmptyState() {
        binding.emptyStateImageView.setVisibility(VISIBLE);
        binding.emptyStateTextView.setVisibility(VISIBLE);
        binding.emptyStateButton.setVisibility(VISIBLE);
    }

}
