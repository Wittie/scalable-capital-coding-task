package wittie.scalable.capital.codingtask;

import javax.inject.Singleton;

import dagger.Component;
import wittie.scalable.capital.codingtask.data.downloaders.CommitsDownloader;
import wittie.scalable.capital.codingtask.data.downloaders.RepositoriesDownloader;
import wittie.scalable.capital.codingtask.presentation.view.MainActivity;
import wittie.scalable.capital.codingtask.presentation.viewmodel.RepositoryViewModel;

@Singleton
@Component(modules = {ApplicationModule.class})
public interface BasicComponent {

    void inject(RepositoriesDownloader downloader);
    void inject(CommitsDownloader downloader);

    void inject(MainActivity activity);

    void inject(RepositoryViewModel viewModel);

}
