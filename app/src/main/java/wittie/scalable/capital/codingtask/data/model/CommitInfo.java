package wittie.scalable.capital.codingtask.data.model;

import com.google.gson.annotations.SerializedName;

public final class CommitInfo {

    private CommitPersonInfo author;
    private CommitPersonInfo committer;
    private String message;
    private Object tree;
    private String url;

    @SerializedName("comment_count")
    private int commentCount;

}
