package wittie.scalable.capital.codingtask.data.downloaders.events;

import wittie.scalable.capital.codingtask.data.model.CommitsListEntry;

public final class OnSuccessDownloadingCommit {

    private final String repositoryName;
    private final CommitsListEntry lastCommit;

    public OnSuccessDownloadingCommit(String repositoryName, CommitsListEntry lastCommit) {
        this.repositoryName = repositoryName;
        this.lastCommit = lastCommit;
    }

    public String getRepositoryName() {
        return repositoryName;
    }

    public CommitsListEntry getLastCommit() {
        return lastCommit;
    }
}

