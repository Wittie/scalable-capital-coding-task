package wittie.scalable.capital.codingtask.data.downloaders;

import android.util.Log;

import org.greenrobot.eventbus.EventBus;

import java.net.UnknownHostException;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import wittie.scalable.capital.codingtask.ScalableCapitalApplication;
import wittie.scalable.capital.codingtask.data.EndPointInterface;
import wittie.scalable.capital.codingtask.data.downloaders.events.OnErrorDownloadingCommit;
import wittie.scalable.capital.codingtask.data.downloaders.events.OnSuccessDownloadingCommit;
import wittie.scalable.capital.codingtask.data.model.CommitsListEntry;
import wittie.scalable.capital.codingtask.domain.RealmDbHandler;

public class CommitsDownloader implements Callback<List<CommitsListEntry>> {
    
    public static final String TAG = CommitsDownloader.class.getSimpleName();

    @Inject
    EventBus eventBus;

    @Inject
    EndPointInterface apiService;

    @Inject
    RealmDbHandler dbHandler;

    private String repositoryName;

    public CommitsDownloader() {
        ScalableCapitalApplication.app().basicComponent().inject(this);
    }

    public void getCommitsForRepository(String repositoryName) {
        this.repositoryName = repositoryName;
        Log.d(TAG, "Getting Commits for: " + repositoryName);
        Call<List<CommitsListEntry>> call = apiService.getCommitsForRepository(repositoryName);
        call.enqueue(this);
    }

    @Override
    public void onResponse(Call<List<CommitsListEntry>> call, Response<List<CommitsListEntry>> response) {
        Log.d(TAG, "onResponse: " + response.body());
        if (response.body() != null) {
            final CommitsListEntry lastCommit = response.body().get(0);
            eventBus.post(new OnSuccessDownloadingCommit(repositoryName, lastCommit));
        }
    }

    @Override
    public void onFailure(Call<List<CommitsListEntry>> call, Throwable t) {
        if (t instanceof UnknownHostException) {
            Log.d(TAG, "No Internet: Are you sure you are connected?");
        } else {
            Log.d(TAG, "onFailure: " + t.getLocalizedMessage());
            eventBus.post(new OnErrorDownloadingCommit(t, repositoryName));
        }
    }
}
