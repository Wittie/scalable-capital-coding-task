package wittie.scalable.capital.codingtask.presentation.view;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import wittie.scalable.capital.codingtask.R;
import wittie.scalable.capital.codingtask.data.model.OnlineRepository;

class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewViewHolder>  {

    private OnlineRepository[] repositories;

    public void setRepositories(OnlineRepository[] repositories) {
        this.repositories = repositories;
        notifyDataSetChanged();
    }

    @Override
    public RecyclerViewViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        ViewDataBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.row_repository, parent, false);
        return new RecyclerViewViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(RecyclerViewViewHolder holder, int position) {
        holder.bind(repositories[position]);
    }

    @Override
    public int getItemCount() {
        return repositories != null ? repositories.length : 0;
    }
}
