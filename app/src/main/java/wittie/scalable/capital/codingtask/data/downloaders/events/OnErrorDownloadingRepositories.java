package wittie.scalable.capital.codingtask.data.downloaders.events;

public final class OnErrorDownloadingRepositories {

    private final Throwable throwable;

    public OnErrorDownloadingRepositories(Throwable throwable) {
        this.throwable = throwable;
    }

    public Throwable getThrowable() {
        return throwable;
    }
}
