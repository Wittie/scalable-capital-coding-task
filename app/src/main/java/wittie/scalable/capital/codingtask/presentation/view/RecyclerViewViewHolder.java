package wittie.scalable.capital.codingtask.presentation.view;

import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;

import wittie.scalable.capital.codingtask.BR;
import wittie.scalable.capital.codingtask.data.model.OnlineRepository;
import wittie.scalable.capital.codingtask.presentation.viewmodel.RepositoryViewModel;

public class RecyclerViewViewHolder extends RecyclerView.ViewHolder {

    private ViewDataBinding itemBinding;
    private RepositoryViewModel viewModel;

    public RecyclerViewViewHolder(ViewDataBinding itemBinding) {
        super(itemBinding.getRoot());
        this.itemBinding = itemBinding;
        this.viewModel = new RepositoryViewModel();
    }

    public void bind(OnlineRepository repository) {
        viewModel.setRepository(repository);
        itemBinding.setVariable(BR.viewModel, viewModel);
        itemBinding.executePendingBindings();
    }
}
