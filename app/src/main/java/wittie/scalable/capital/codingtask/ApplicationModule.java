package wittie.scalable.capital.codingtask;

import android.content.Context;

import org.greenrobot.eventbus.EventBus;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.realm.Realm;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import wittie.scalable.capital.codingtask.data.EndPointInterface;
import wittie.scalable.capital.codingtask.data.downloaders.CommitsDownloader;
import wittie.scalable.capital.codingtask.data.downloaders.RepositoriesDownloader;
import wittie.scalable.capital.codingtask.domain.RealmDbHandler;

@Module
public class ApplicationModule {

    private final Context context;

    public ApplicationModule(Context context) {
        this.context = context;
    }

    @Provides
    @Singleton
    Context providesContext() {
        return context;
    }

    @Provides
    @Singleton
    Realm providesRealmDefaultInstance() {
        return Realm.getDefaultInstance();
    }

    @Provides
    @Singleton
    RealmDbHandler providesDataBaseHandler() {
        return new RealmDbHandler(providesRealmDefaultInstance());
    }

    @Provides
    @Singleton
    RepositoriesDownloader providesRepositoriesDownloader() {
        return new RepositoriesDownloader();
    }

    @Provides
    CommitsDownloader providesCommitsDownloader() {
        return new CommitsDownloader();
    }

    @Provides
    @Singleton
    EventBus providesEventBus() {
        return EventBus.getDefault();
    }

    @Provides
    @Singleton
    EndPointInterface providesEndPointInterface() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(EndPointInterface.SERVER)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        return retrofit.create(EndPointInterface.class);
    }

}