package wittie.scalable.capital.codingtask.data;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import wittie.scalable.capital.codingtask.data.model.CommitsListEntry;
import wittie.scalable.capital.codingtask.data.model.OnlineRepository;

public interface EndPointInterface {

    String SERVER = "https://api.github.com/";
    String ENDPOINT_REPOSITORIES = "users/mralexgray/repos";
    String ENDPOINT_COMMITS = "repos/mralexgray/{user}/commits";

    @GET(ENDPOINT_REPOSITORIES)
    Call<List<OnlineRepository>> getOnlineRepositories();

    @GET(ENDPOINT_COMMITS)
    Call<List<CommitsListEntry>> getCommitsForRepository(@Path("user") String user);

}
